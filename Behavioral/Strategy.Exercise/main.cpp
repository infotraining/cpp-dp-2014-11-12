#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <memory>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

typedef std::vector<StatResult> Results;

enum StatisticsType
{
	AVG, MINMAX, SUM
};

class Statistics
{
public:
    virtual void calculate(const std::vector<double>& data, Results& results) = 0;

    virtual ~Statistics() {}
};

using StatisticsPtr = std::shared_ptr<Statistics>;

class Avg : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.push_back(result);
    }
};

class Min : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double min = *(std::min_element(data.begin(), data.end()));

        results.push_back(StatResult("MIN", min));
    }
};

class Max : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double max = *(std::max_element(data.begin(), data.end()));

        results.push_back(StatResult("MAX", max));
    }
};


class Sum : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.push_back(StatResult("SUM", sum));
    }
};

class StatGroup : public Statistics
{
    std::vector<StatisticsPtr> stats_;
public:
    void calculate(const std::vector<double>& data, Results& results) override
    {
        for(const auto& stat : stats_)
            stat->calculate(data, results);
    }

    void add(StatisticsPtr statistics)
    {
        stats_.push_back(statistics);
    }
};


class DataAnalyzer
{
    StatisticsPtr statistics_;
	std::vector<double> data_;
public:
    DataAnalyzer(StatisticsPtr statistics) : statistics_{statistics}
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

	void save_data(const std::string& file_name) const
	{ 
		std::ofstream fout(file_name.c_str());
		if (!fout)
			throw std::runtime_error("File not opened");

		for(std::vector<double>::const_iterator it = data_.begin(); it != data_.end(); ++it)
			fout << (*it) << std::endl;
	}

    void set_statistics(StatisticsPtr statistics)
	{
        statistics_ = statistics;
	}

	void calculate(Results& results)
	{
        statistics_->calculate(data_, results);
	}
};

void print_results(const Results& results)
{
    for(Results::const_iterator it = results.begin(); it != results.end(); ++it)
		std::cout << it->description << " = " << it->value << std::endl;
}

int main()
{
    auto AVG = std::make_shared<Avg>();
    auto MIN = std::make_shared<Min>();
    auto MAX = std::make_shared<Max>();
    auto MINMAX = std::make_shared<StatGroup>();
    MINMAX->add(MIN);
    MINMAX->add(MAX);
    auto SUM = std::make_shared<Sum>();

    auto STD_GROUP = std::make_shared<StatGroup>();
    STD_GROUP->add(AVG);
    STD_GROUP->add(MINMAX);
    STD_GROUP->add(SUM);

	Results results;

    DataAnalyzer da(STD_GROUP);
	da.load_data("data.dat");
	da.calculate(results);

//	da.set_statistics(MINMAX);
//	da.calculate(results);

//	da.set_statistics(SUM);
//	da.calculate(results);

	print_results(results);

	std::cout << "\n\n";

	results.clear();
	da.load_data("new_data.dat");
	da.calculate(results);

	print_results(results);
}
