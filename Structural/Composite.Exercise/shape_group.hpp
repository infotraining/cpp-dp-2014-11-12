#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <list>
#include <memory>



namespace Drawing
{

//class Person
//{
//    int id_;
//    std::string name_;
//public:
//    Person(int id, const std::string& name) : id_{id}, name_{name}
//    {}
//};

//void emplace_back_demo()
//{
//    std::list<Person> people;

//    people.push_back(Person{1, "Kowalski"});
//    people.emplace_back(2, "Nowak");
//}

// TO DO: zaimplementowac kompozyt grupujący kształty geometryczne
class ShapeGroup : public Shape
{
    std::list<std::shared_ptr<Shape>> shapes_;
public:
    ShapeGroup() = default;

    ShapeGroup(const ShapeGroup& source)
    {
        for(const auto& s : source.shapes_)
        //  shapes_.emplace_back(s->clone());
            shapes_.push_back(std::shared_ptr<Shape>{s->clone()});
    }

    void draw() const override
    {
        for(const auto& s : shapes_)
            s->draw();
    }

    void move(int dx, int dy) override
    {
        for(const auto& s : shapes_)
            s->move(dx, dy);
    }

    ShapeGroup* clone() const override
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream& in) override
    {
        size_t count;
        in >> count;

        std::string type_id;

        for(size_t i = 0; i < count; ++i)
        {
            in >> type_id;

            auto shape = std::shared_ptr<Shape> {ShapeFactory::instance().create(type_id)};
            shape->read(in);
            add(shape);
        }
    }

    void write(std::ostream& out) override
    {
        out << shapes_.size();
        for(const auto& s : shapes_)
            s->write(out);
    }

    void add(std::shared_ptr<Shape> shape)
    {
        shapes_.push_back(shape);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
