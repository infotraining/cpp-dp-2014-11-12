#ifndef PROXY_HPP_
#define PROXY_HPP_

#include <iostream>
#include <string>
#include <memory>

// "Subject" 
class Subject
{
public:
	virtual void request() = 0;
    virtual ~Subject() = default;
};

// "RealSubject"
class RealSubject : public Subject
{
public:
	RealSubject()
	{
		std::cout << "RealSubject's creation" << std::endl;
	}

    RealSubject(const RealSubject&) = delete;
    RealSubject& operator=(const RealSubject&) = delete;

	~RealSubject()
	{
		std::cout << "RealSubject's clean-up" << std::endl;
	}
	
    void request() override
	{
		std::cout << "Called RealSubject.request()" << std::endl;
	}
};

// "Proxy" 
class Proxy : public Subject
{
    std::unique_ptr<RealSubject> real_subject_;
	
public:
    Proxy() : real_subject_{nullptr}
	{
		std::cout << "Proxy's creation" << std::endl;
	}

	void request()
	{
		// lazy initialization'
        if (!real_subject_)
		{
            real_subject_.reset(new RealSubject());
		}

		real_subject_->request();
	}
};

#endif /*PROXY_HPP_*/
