#ifndef SQUARE_HPP_
#define SQUARE_HPP_

// TODO: Dodać klase Square

#include "shape.hpp"

namespace Drawing
{
    class Square : public ShapeBase
    {
        size_t size_;
    public:
        Square(int x = 0, int y = 0, size_t size = 0) : ShapeBase{x, y}, size_{size}
        {}

        void draw() const
        {
            std::cout << "Drawing Square at: " << point() << " with size: " << size_ << std::endl;
        }

        void read(std::istream& in) override
        {
            Point pt;
            size_t size;

            in >> pt >> size;

            set_point(pt);
            size_ = size;
        }

        void write(std::ostream& out) override
        {
            out << "Square " << point() << " " << size_ << std::endl;
        }
    };
};

#endif /* SQUARE_HPP_ */
