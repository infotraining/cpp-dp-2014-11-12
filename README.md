# reinstalacja dodatków do VBox

    sudo /etc/init.d/vboxadd setup


# ustawienie proxy

    Najwygodniej jest dodac wpis w pliku .profile

    export http_proxy=http://10.144.1.10:8080
    export https_proxy=https://10.144.1.10:8080


# ustawienie proxy dla APT

    /etc/apt/apt.conf

    Acquire::http::proxy "http://10.144.1.10:8080/";
    Acquire::https::proxy "https://10.144.1.10:8080/";
    Acquire::ftp::proxy "ftp://10.144.1.10:8080/";


# test polaczenia

    ping 8.8.8.8 # przy zalozeniu, ze ICMP nie jest blokowane
    curl -v http://www.example.com/


# git

    git-cheat-sheet
    http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf

    git status  # status lokalnego repozytorium
    git pull    # pobranie plikow z repozytorium
    git stash   # przeniesienie modyfikacji do schowka